This proposal uses the [minted package](http://mirror.math.ku.edu/tex-archive/macros/latex/contrib/minted/minted.pdf)
to format C code. This requires installation of [Pygments](https://pypi.python.org/pypi/Pygments).
